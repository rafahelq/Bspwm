#!/bin/bash

if [ "$(whoami)" == "root" ]; then
    exit 1
fi

echo "Instalar y configurar BSPWM"
echo "partiendo de una instalación mínima de"
echo "Debian GNU/Linux 12 - Bookworm - 64 bits"

sudo apt update

# instalar servidor gráfico y bspwm
sudo apt install -y xorg xterm lightdm lightdm-gtk-greeter bspwm sxhkd polybar rofi jgmenu suckless-tools tint2 feh nitrogen terminator picom conky dunst nm-tray nm-tray-l10n xtitle xfce4-terminal thunar arandr gdebi curl htop git wget inxi vim build-essential devscripts neofetch glances


# descargar repositorio gitlab
cd /tmp/
git clone https://gitlab.com/linux-en-casa/bspwm.git
cd /tmp/bspwm/
git branch -a
git checkout -b bspwm-color origin/bspwm-color 
git pull origin bspwm-color 
mkdir -p ~/.config
cp -r * ~/.config/

# descargar fuentes de nerdfonts
echo "El tema utiliza las fuentes nerd-fonts"
echo "Se descargan de github"
sleep 2
cd /tmp
git clone --depth 1 https://github.com/ryanoasis/nerd-fonts.git
/tmp/nerd-fonts/install.sh 


# instalar mas paquetes
sudo apt install -y firefox-esr gimp isenkram-cli clementine  obs-studio policykit-1-gnome libreoffice transmission libspa-0.2-bluetooth vlc gedit volumeicon-alsa flameshot ristretto mpv xfce4-screenshooter synaptic

# crear directorios de usuarios
sudo apt install -y xdg-user-dirs
xdg-user-dirs-update 

# paquetes adicionales
sudo apt install -y libreoffice-gtk3 libreoffice-l10n-es 

# para algunas apps como synaptic
sudo apt install -y software-properties-common software-properties-gtk

# pipewire
sudo apt install -y wireplumber pipewire-media-session- pipewire-pulse pipewire-alsa libspa-0.2-bluetooth 


# Mensaje de Instalado

echo "BSPWM Instalado"
sleep 2
echo "..."
echo "..."
echo "fin de instalación"

