#!/bin/bash

if [ "$(whoami)" == "root" ]; then
    exit 1
fi

echo "Instalar y configurar BSPWM"
echo "en Ubuntu - solo instalo los paquetes de bspwm"
echo "y algunos programas por cli"
sudo apt update

# instalar servidor gráfico y bspwm
sudo apt install -y bspwm sxhkd polybar rofi jgmenu suckless-tools tint2 feh nitrogen terminator picom conky dunst nm-tray nm-tray-l10n xtitle xfce4-terminal thunar arandr gdebi curl htop git wget inxi build-essential neofetch glances volumeicon-alsa policykit-1-gnome


# descargar repositorio gitlab
cd /tmp/
git clone https://gitlab.com/linux-en-casa/bspwm.git
cd /tmp/bspwm/
git branch -a
git checkout -b bspwm-color origin/bspwm-color 
git pull origin bspwm-color 

cp -r * ~/.config/

# descargar fuentes de nerdfonts
echo "El tema utiliza las fuentes nerd-fonts"
echo "Se descargan de github"
sleep 2
cd /tmp
git clone --depth 1 https://github.com/ryanoasis/nerd-fonts.git
/tmp/nerd-fonts/install.sh 


# Mensaje de Instalado

echo "BSPWM Instalado"
sleep 2
echo "..."
echo "..."
echo "fin de instalación"

